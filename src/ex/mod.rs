/// A function that does something. Make sure you include example code where appropriate.
///
/// ```
/// use rctfd_template::ex::something;
/// assert_eq!(something(), "Hello, world!");
/// ```
pub fn something() -> &'static str {
    "Hello, world!"
}
