//! Template for new Rust repositories in RCTFd!
//!
//! Ensure that you are using this as part of your new repositories.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

#[cfg(test)]
mod test;

/// The ex module for rctfd_repo_template. Does something with something()!
pub mod ex;

/// Does a really hard problem :hmm:.
pub fn hard_problem() -> u8 {
    2 + 2
}
