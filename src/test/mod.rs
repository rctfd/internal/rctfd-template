//! Test suites (tests for each module) should be modularised in the structure matching that of
//! the src/ directory.
//! Tests for the root module should be placed in mod.rs of test.

use crate::hard_problem;

/// Tests for the ex module.
mod ex;

/// Ensures that rctfd_repo_template::hard_problem() returns the value correctly.
#[test]
fn test_hard_problem() {
    assert_eq!(hard_problem(), 4);
}
