use crate::ex::something;

/// Ensures that reversal of the string matches too.
#[test]
fn reverse() {
    assert_eq!(
        something().chars().rev().collect::<String>(),
        "Hello, world!".chars().rev().collect::<String>()
    );
}
