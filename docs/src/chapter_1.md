# Using RCTFd Template

1. Clone this repository: `git clone https://gitlab.com/rctfd/internal/rctfd-template.git new-project`
2. Obliterate the history: `cd new-project; rm -rf .git; git init`
3. Add the hooks: `./hooks/init-hooks.sh`
4. Update the Cargo.toml with the authors and project name
5. Update the README.md as appropriate (you can figure that out)
6. Remove the example code and replace it with yours (retain `forbid` and `deny` statements for compliance)
7. Update the user documentation appropriately (`book.toml`, remove chapters, update SUMMARY)
8. Update the `.gitlab-ci.yml` to only have the build stages you need (some repos may not need release, for example)
9. Add and commit your changes: `git add -A; git commit -m "Initial commit"`
10. Add your remote: `git remote add origin git@gitlab.com:example/new-project.git`
11. `git push -u origin master`
