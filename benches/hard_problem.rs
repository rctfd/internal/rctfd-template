use criterion::{criterion_group, criterion_main, Criterion};
use rctfd_template::hard_problem;

fn hard_problem_benchmark(c: &mut Criterion) {
    c.bench_function("hard_problem", |b| b.iter(hard_problem));
}

criterion_group!(benches, hard_problem_benchmark);
criterion_main!(benches);
