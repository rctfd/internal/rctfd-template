use rctfd_template::hard_problem;

fn main() {
    // compute 2 + 2
    let x = hard_problem();
    for i in 0..10 {
        if x == i {
            return;
        }
    }
    panic!("We didn't solve the problem!")
}
