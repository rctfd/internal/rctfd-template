use rctfd_template::hard_problem;

/// Make sure the hard problem can be done using a value derived somewhere else (integ test).
#[test]
fn ensure_hard_problem() {
    assert_eq!(hard_problem(), 2 + 2);
}
