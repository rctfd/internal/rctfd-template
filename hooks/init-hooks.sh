#!/usr/bin/env bash

# Assumes that this script is inside the target git repository as hooks/init-hooks.sh
# This file is licensed under WTFPL: http://www.wtfpl.net/txt/copying/

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "$DIR"
DIR="$(git rev-parse --show-toplevel)"
cd "$DIR/.git/hooks"

for i in $(find ../../hooks -path '../../hooks/init-hooks.sh' -prune -o -type f -print); do
  ln -si "$i"
done
