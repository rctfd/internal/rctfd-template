# REPOSITORY NAME HERE

[![pipeline status](https://gitlab.com/rctfd/internal/rctfd-template/badges/master/pipeline.svg)](https://gitlab.com/rctfd/internal/rctfd-template/-/commits/master)
[![coverage report](https://gitlab.com/rctfd/internal/rctfd-template/badges/master/coverage.svg)](https://gitlab.com/rctfd/internal/rctfd-template/-/commits/master)

Brief description of repository and purpose.

## Version (vX.X.X) Release Notes

- A brief rundown of changes introduced in this release.
- This should cover all major changes and bugfixes.
- Include any details that would compromise backwards compatibility.
- Make sure these are replicated (and retained) in [CHANGELOG](CHANGELOG.md)

## Documentation

[Documentation][docs] has been made available for this repository.

[docs]: https://rctfd.gitlab.io/internal/rctfd-template/

## Developer Dependencies

To develop this software, you'll need to install rust via rustup, then:
 - `rustup component add clippy`
 - `cargo install cargo-audit`
 - `cargo install mdbook`
 - `./hooks/init-hooks.sh`

This way, any contributions you make will be checked before you even submit a
pull request!
